/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 19:37:31 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/20 19:14:55 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	ft_lst_push_front(t_list **alst, t_list *new)
{
	t_list *tmp;

	tmp = *alst;
	*alst = new;
	new->next = tmp;
}
