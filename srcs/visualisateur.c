/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   graph.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/10 13:38:13 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/14 13:44:04 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "visualisateur.h"

static void	display_board(t_env *env)
{
	int		i;
	int		col;

	if (ft_strstr(env->buff, "Plateau"))
	{
		werase(env->board);
		box(env->board, ACS_VLINE, ACS_HLINE);
		i = 1;
		col = 1;
		while (!ft_strstr(env->buff, "Piece"))
		{
			mvwprintw(env->board, i++, col, env->buff);
			if (i >= LINES - 1)
			{
				i = 2;
				col = COLS / 3;
			}
			getstr(env->buff);
		}
		wrefresh(env->board);
	}
}

static void	display_score(t_env *env)
{
	if (ft_strstr(env->buff, "=="))
	{
		mvwprintw(env->score, 1, 1, env->buff);
		getstr(env->buff);
		mvwprintw(env->score, 2, 1, env->buff);
		wrefresh(env->score);
		env->end = 1;
		getstr(env->buff);
	}
}

static void	display_piece(t_env *env)
{
	int		j;
	int		col;

	if (ft_strstr(env->buff, "Piece"))
	{
		werase(env->piece);
		box(env->piece, ACS_VLINE, ACS_HLINE);
		j = 1;
		col = 1;
		while (!ft_strstr(env->buff, "<got"))
		{
			mvwprintw(env->piece, j++, col, env->buff);
			getstr(env->buff);
		}
		if (ft_strstr(env->buff, "<got"))
		{
			mvwprintw(env->piece, j++, col, env->buff);
			getstr(env->buff);
			j++;
			display_score(env);
		}
		wrefresh(env->piece);
	}
}

static void	init_screen(t_env *env)
{
	initscr();
	env->board = subwin(stdscr, LINES, (COLS * 2) / 3, 0, 0);
	env->piece = subwin(stdscr, (LINES * 2) / 3, COLS / 3, 0, (COLS * 2) / 3);
	env->score =
		subwin(stdscr, LINES / 3, COLS / 3, (LINES * 2) / 3, (COLS * 2) / 3);
	box(env->board, ACS_VLINE, ACS_HLINE);
	box(env->piece, ACS_VLINE, ACS_HLINE);
	box(env->score, ACS_VLINE, ACS_HLINE);
	noecho();
	env->end = 0;
}

int			main(void)
{
	t_env	env;

	init_screen(&env);
	getstr(env.buff);
	while (env.end == 0)
	{
		display_board(&env);
		display_piece(&env);
		if (!ft_strstr(env.buff, "Plateau"))
			getstr(env.buff);
	}
	endwin();
	free(env.board);
	free(env.piece);
	free(env.score);
	while (1)
		env.end = 1;
	return (0);
}
