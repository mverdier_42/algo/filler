/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/05 18:36:37 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/13 19:00:29 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"
#include "fill_board.h"

static void	calc_piece_offset(t_round *rd)
{
	rd->offpx = 0;
	while (rd->offpx < rd->px)
	{
		rd->offpy = 0;
		while (rd->offpy < rd->py && rd->piece[rd->offpx][rd->offpy] == '.')
			rd->offpy++;
		if (rd->offpy == rd->py)
			rd->offpx++;
		else
			return ;
	}
}

void		opp_is_alive(t_round *rd)
{
	int		i;
	int		j;
	int		nopp;

	nopp = 0;
	i = 0;
	while (i < rd->bx)
	{
		j = 0;
		while (j < rd->by)
		{
			if (rd->board[i][j] == rd->opp || rd->board[i][j] == rd->opp + 32)
				nopp++;
			j++;
		}
		i++;
	}
	if (nopp > rd->nopp)
		rd->nopp = nopp;
	else
		rd->opp_is_alive = 0;
}

int			save_piece(char **line, t_round *rd)
{
	int			i;

	rd->px = ft_atoi(ft_strchr(*line, ' ') + 1);
	rd->py = ft_atoi(ft_strrchr(*line, ' ') + 1);
	if ((rd->piece = (char**)malloc(sizeof(char*) * rd->px)) == NULL)
		return (-1);
	free(*line);
	i = 0;
	while (i < rd->px && get_next_line(0, line))
	{
		if ((rd->piece[i++] = ft_strdup(*line)) == NULL)
		{
			free(*line);
			return (-1);
		}
		free(*line);
	}
	calc_piece_offset(rd);
	rd->dist = rd->bx + rd->by;
	rd->fx = 0;
	rd->fy = 0;
	return (1);
}

int			save_round_board(char **line, t_round *rd)
{
	int			tmp;

	tmp = 0;
	rd->bx = ft_atoi(ft_strchr(*line, ' ') + 1);
	rd->by = ft_atoi(ft_strrchr(*line, ' ') + 1);
	if ((rd->board = (char**)malloc(sizeof(char*) * rd->bx)) == NULL)
	{
		free(*line);
		return (-1);
	}
	free(*line);
	get_next_line(0, line);
	free(*line);
	while (get_next_line(0, line) && tmp < rd->bx)
	{
		if (!ft_strchr(*line, ' '))
		{
			free(*line);
			return (-1);
		}
		rd->board[tmp++] = ft_strdup(ft_strchr(*line, ' ') + 1);
		free(*line);
	}
	return (1);
}
