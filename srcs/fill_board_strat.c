/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_board_strat.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/05 17:29:12 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/14 13:42:24 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_board.h"

static int	get_next_star(t_round *rd)
{
	int		i;

	i = 0;
	while (rd->starx < rd->px)
	{
		while (rd->stary < rd->py && rd->piece[rd->starx][rd->stary] == '.')
			rd->stary++;
		if (rd->stary >= rd->py)
		{
			rd->starx++;
			rd->stary = 0;
		}
		else
			return (1);
		i++;
	}
	return (0);
}

static void	calc_dist(t_round *rd, int *d, int i, int j)
{
	int		x;
	int		y;

	rd->starx = 0;
	rd->stary = 0;
	if (rd->board[i][j] == rd->opp || rd->board[i][j] == rd->opp + 32)
	{
		while (get_next_star(rd))
		{
			x = (rd->cx - (rd->sx - rd->offpx) + (rd->starx - rd->offpx)) - i;
			y = (rd->cy - (rd->sy - rd->offpy) + (rd->stary - rd->offpy)) - j;
			if ((ABS(x) + ABS(y)) < *d)
				*d = (ABS(x) + ABS(y));
			rd->stary++;
		}
	}
}

void		calc_min_dist(t_round *rd)
{
	int		i;
	int		j;
	int		d;

	d = rd->bx + rd->by;
	i = 0;
	while (i < rd->bx)
	{
		j = 0;
		while (j < rd->by)
		{
			calc_dist(rd, &d, i, j);
			j++;
		}
		i++;
	}
	if (d < rd->dist)
	{
		rd->dist = d;
		rd->fx = rd->cx - rd->sx;
		rd->fy = rd->cy - rd->sy;
	}
}
