/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_board_verifs.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/05 13:21:04 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/14 13:42:41 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_board.h"

int		is_out_of_board(t_round *rd, int x, int y)
{
	if (rd->cx - rd->sx + x < 0 || rd->cx - rd->sx + x >= rd->bx
			|| rd->cy - rd->sy + y < 0 || rd->cy - rd->sy + y >= rd->by)
		if (rd->piece[x][y] == '*')
			return (1);
	return (0);
}

int		is_in_collision(t_round *rd, int *collision, int x, int y)
{
	int		i;
	int		j;

	i = rd->cx - rd->sx + x;
	j = rd->cy - rd->sy + y;
	if (i < rd->bx && j < rd->by && i >= 0 && j >= 0
			&& (rd->board[i][j] == rd->me || rd->board[i][j] == rd->me + 32)
			&& rd->piece[x][y] == '*')
		(*collision)++;
	else if (i < rd->bx && j < rd->by && i >= 0 && j >= 0
			&& (rd->board[i][j] == rd->opp || rd->board[i][j] == rd->opp + 32)
			&& rd->piece[x][y] == '*')
		return (0);
	return (1);
}
