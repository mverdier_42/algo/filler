/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/18 13:36:37 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/14 13:49:05 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "parsing.h"
#include "fill_board.h"
#include "del.h"

static void	players_init(char *line, t_round *rd, int *i)
{
	if (*i != 0)
		return ;
	if (ft_strstr(line, "$$$ exec p1"))
	{
		rd->me = 'O';
		rd->opp = 'X';
		rd->opp_is_alive = 1;
		rd->nopp = 0;
		(*i)++;
	}
	else if (ft_strstr(line, "$$$ exec p2"))
	{
		rd->me = 'X';
		rd->opp = 'O';
		rd->opp_is_alive = 1;
		rd->nopp = 0;
		(*i)++;
	}
}

static int	get_board(char **line, t_round *rd)
{
	if (ft_strstr(*line, "Plateau "))
	{
		if (save_round_board(line, rd) < 0)
			return (-1);
		if (rd->opp_is_alive == 1)
			opp_is_alive(rd);
		return (1);
	}
	return (0);
}

static void	print_coords(t_round *rd)
{
	if (rd->opp_is_alive == 1)
	{
		get_coords(rd);
		ft_printf("%d %d\n", rd->fx, rd->fy);
	}
	else if (fill_to_end(rd) == 1)
		ft_printf("%d %d\n", rd->cx - rd->sx, rd->cy - rd->sy);
	else
		ft_printf("0 0\n");
	del_piece(rd);
	del_board(rd);
}

int			main(void)
{
	char	*line;
	int		i;
	t_round	rd;

	i = 0;
	while (get_next_line(0, &line))
	{
		players_init(line, &rd, &i);
		if (get_board(&line, &rd) < 0)
			return (-1);
		if (ft_strstr(line, "Piece "))
		{
			if (save_piece(&line, &rd) < 0)
			{
				return (-1);
				del_board(&rd);
			}
			print_coords(&rd);
		}
		else
			free(line);
	}
	free(line);
	return (0);
}
