/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:51:18 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/04 16:48:25 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

char	*ft_strnew(size_t size)
{
	char	*newstr;

	if ((newstr = (char *)malloc(sizeof(*newstr) * size + 1)) == NULL)
		return (NULL);
	ft_bzero(newstr, size + 1);
	return (newstr);
}
