/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/14 10:44:06 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/16 12:33:19 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*buf_transfer_n(t_buf *lst, char **line)
{
	int i;

	i = 0;
	while (i < lst->nb && lst->buf[i] != '\n')
		i++;
	if ((*line = ft_nrealloc(*line, (lst->line_len + i) + 1,
					lst->line_len)) == NULL)
		return (NULL);
	ft_memcpy((*line + lst->line_len), lst->buf, i);
	(*line)[lst->line_len + i] = '\0';
	lst->line_len += i;
	ft_memmove(lst->buf, ft_memchr(lst->buf, '\n', lst->nb) + 1,
			BUFF_SIZE - (i + 1));
	lst->nb -= (i + 1);
	return (*line);
}

char	*buf_transfer_nb(t_buf *lst, char **line)
{
	if ((*line = ft_nrealloc(*line, (lst->line_len + lst->nb) + 1,
					lst->line_len)) == NULL)
		return (NULL);
	ft_memcpy((*line + lst->line_len), lst->buf, lst->nb);
	(*line)[lst->line_len + lst->nb] = '\0';
	lst->line_len += lst->nb;
	return (*line);
}

int		get_next_line_r(t_buf *lst, char **line)
{
	int			tmp;

	if ((ft_memchr(lst->buf, '\n', lst->nb)))
	{
		if ((*line = buf_transfer_n(lst, line)) == NULL)
			return (-1);
		lst->line_len = 0;
		return (1);
	}
	if ((*line = buf_transfer_nb(lst, line)) == NULL)
		return (-1);
	tmp = lst->nb;
	lst->nb = read(lst->fd, lst->buf, BUFF_SIZE);
	if (tmp == 0 && lst->nb == 0)
		return (0);
	else if (lst->nb < 0)
	{
		lst->nb = 0;
		return (-1);
	}
	else if (lst->nb > 0)
		get_next_line_r(lst, line);
	lst->line_len = 0;
	return (1);
}

t_buf	*my_lst_push_back_new(t_buf **lst, const int fd)
{
	t_buf	*new;
	t_buf	*tmp;

	tmp = *lst;
	if ((new = (t_buf*)malloc(sizeof(t_buf))) == NULL)
		return (NULL);
	new->fd = fd;
	new->nb = 0;
	new->line_len = 0;
	new->next = NULL;
	if (!*lst)
		return (*lst = new);
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = new;
	return (new);
}

int		get_next_line(const int fd, char **line)
{
	static t_buf	*lst = NULL;
	t_buf			*tmp;

	if (fd < 0 || line == NULL)
		return (-1);
	*line = NULL;
	tmp = lst;
	if (!lst || (fd != lst->fd && !lst->next))
		return (get_next_line_r(my_lst_push_back_new(&lst, fd), line));
	while (fd != tmp->fd && tmp->next)
		tmp = tmp->next;
	if (fd == tmp->fd)
		return (get_next_line_r(tmp, line));
	else
		return (get_next_line_r(my_lst_push_back_new(&lst, fd), line));
}
