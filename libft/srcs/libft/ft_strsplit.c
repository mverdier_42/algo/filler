/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:51:45 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/07 16:35:12 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

static int		ft_countword(char const *s, char c)
{
	int i;
	int w;

	i = 0;
	w = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		while (s[i] && s[i] != c)
		{
			i++;
			if (s[i] == c || s[i] == '\0')
				w++;
		}
	}
	return (w);
}

static char		**ft_wordalloc(char const *s, char c, char **newtab)
{
	int i;
	int len;
	int w;

	i = 0;
	w = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		len = 0;
		while (s[i] && s[i] != c)
		{
			i++;
			len++;
			if (s[i] == c || s[i] == '\0')
			{
				if ((newtab[w] = ft_strnew(len + 1)) == NULL)
					return (NULL);
				w++;
			}
		}
	}
	return (newtab);
}

static char		**ft_fillit(char const *s, char c, char **newtab)
{
	int i;
	int j;
	int w;

	i = 0;
	w = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		j = 0;
		while (s[i] && s[i] != c)
		{
			newtab[w][j] = s[i];
			i++;
			j++;
			if (s[i] == c || s[i] == '\0')
				w++;
		}
	}
	if (i == 0 || !s[i])
		newtab[w] = NULL;
	return (newtab);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**newtab;
	int		word;

	if (!s)
		return (NULL);
	word = ft_countword(s, c);
	if ((newtab = (char **)malloc(sizeof(*newtab) * word + 1)) == NULL)
		return (NULL);
	newtab = ft_memset(newtab, 0, word);
	if ((newtab = ft_wordalloc(s, c, newtab)) == NULL)
		return (NULL);
	newtab = ft_fillit(s, c, newtab);
	return (newtab);
}
