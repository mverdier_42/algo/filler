GREEN=`echo '\033[1;32m'`
RED=`echo '\033[1;31m'`
RESET=`echo '\033[0m'`
if [ $3 = 2 ]; then
	../resources/filler_vm -f ../resources/maps/$1 -p1 ../mverdier.filler -p2 ../resources/players/$2 | ./visualisateur.filler | sed -e 's/[oO]/'"$GREEN"'&'"$RESET"'/g'
else
	../resources/filler_vm -f ../resources/maps/$1 -p2 ../mverdier.filler -p1 ../resources/players/$2 | ./visualisateur.filler | sed -e 's/[oO]/'"$RED"'&'"$RESET"'/g'
fi
