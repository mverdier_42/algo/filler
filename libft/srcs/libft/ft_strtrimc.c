/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrimc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/06 14:09:30 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/06 14:12:56 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

static char	*ft_stralloc(size_t i, size_t s_len)
{
	char *newstr;

	if (i >= s_len)
	{
		if ((newstr = ft_strnew(0)) == NULL)
			return (NULL);
	}
	else
	{
		if ((newstr = ft_strnew(s_len - i + 1)) == NULL)
			return (NULL);
	}
	if (i > s_len)
		newstr[0] = '\0';
	return (newstr);
}

char		*ft_strtrimc(char const *s, int c)
{
	char	*newstr;
	size_t	i;
	size_t	j;
	size_t	s_len;

	s_len = ft_strlen(s);
	i = 0;
	j = 0;
	while (s[i] && s[i] == c)
		i++;
	while ((s[s_len] == c || s[s_len] == '\0') && s_len > 0)
		s_len--;
	if ((newstr = ft_stralloc(i, s_len)) == NULL)
		return (NULL);
	while (i < s_len)
	{
		newstr[j] = s[i];
		i++;
		j++;
	}
	if (i == s_len)
		newstr[j] = s[i];
	return (newstr);
}
