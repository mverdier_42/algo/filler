/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/22 15:52:39 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/13 18:34:15 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSING_H
# define PARSING_H

# include "libft.h"

typedef struct	s_round
{
	char	**piece;
	char	**board;
	char	me;
	char	opp;
	int		nopp;
	int		opp_is_alive;
	int		px;
	int		py;
	int		bx;
	int		by;
	int		offpx;
	int		offpy;
	int		sx;
	int		sy;
	int		cx;
	int		cy;
	int		fx;
	int		fy;
	int		starx;
	int		stary;
	int		dist;
}				t_round;

int				save_piece(char **line, t_round *rd);
int				save_round_board(char **line, t_round *rd);
void			opp_is_alive(t_round *rd);

#endif
