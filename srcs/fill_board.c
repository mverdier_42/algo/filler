/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_board.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/05 18:36:47 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/14 15:35:59 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_board.h"

static int	try_to_place(t_round *rd)
{
	int		x;
	int		y;
	int		collision;

	collision = 0;
	x = 0;
	while (x < rd->px)
	{
		y = 0;
		while (y < rd->py)
		{
			if (is_out_of_board(rd, x, y) == 1)
				return (0);
			if (!is_in_collision(rd, &collision, x, y) || collision > 1)
				return (0);
			y++;
		}
		x++;
	}
	if (collision != 1)
		return (0);
	return (1);
}

int			to_next_star(t_round *rd)
{
	int		i;

	i = 0;
	while (rd->sx < rd->px)
	{
		while (rd->sy < rd->py && rd->piece[rd->sx][rd->sy] == '.')
			rd->sy++;
		if (rd->sy >= rd->py)
		{
			rd->sx++;
			rd->sy = 0;
		}
		else
			return (1);
		i++;
	}
	return (0);
}

void		get_coords(t_round *rd)
{
	rd->cx = 0;
	while (rd->cx < rd->bx)
	{
		rd->cy = 0;
		while (rd->cy < rd->by)
		{
			if (rd->board[rd->cx][rd->cy] == rd->me
					|| rd->board[rd->cx][rd->cy] == rd->me + 32)
			{
				rd->sx = 0;
				rd->sy = 0;
				while (to_next_star(rd) == 1)
				{
					if (try_to_place(rd) == 1)
						calc_min_dist(rd);
					rd->sy++;
				}
			}
			rd->cy++;
		}
		rd->cx++;
	}
}

int			fill_to_end(t_round *rd)
{
	rd->sx = 0;
	rd->sy = 0;
	while (to_next_star(rd) == 1)
	{
		rd->cx = 0;
		while (rd->cx < rd->bx)
		{
			rd->cy = 0;
			while (rd->cy < rd->by)
			{
				if (rd->board[rd->cx][rd->cy] == rd->me
						|| rd->board[rd->cx][rd->cy] == rd->me + 32)
					if (try_to_place(rd) == 1)
						return (1);
				rd->cy++;
			}
			rd->cx++;
		}
		rd->sy++;
	}
	return (0);
}
