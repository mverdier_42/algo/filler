/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_calc_digits.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:58:35 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:58:42 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_size.h"

int		calc_int(t_params *param, va_list ap, t_args **args)
{
	t_args	*arg;

	arg = push_back_new_arg(args);
	if (param->type == 'D' || param->size == 'l')
		arg->type = LINT;
	else if (param->type != 'D' && param->size == 'L')
		arg->type = LLINT;
	else if (param->type != 'D' && (param->size == 'h' || param->size == 'H'))
	{
		arg->type = INT;
		if (param->size == 'h')
			arg->stype = SINT;
		else
			arg->stype = SCHAR;
	}
	else if (param->type != 'D' && param->size == 'j')
		arg->type = INTMAX_T;
	else if (param->type != 'D' && param->size == 'z')
		arg->type = SSIZE_T;
	else if (param->type != 'D')
		arg->type = INT;
	return ((arg->len = ft_intmax_len(save_intmax_arg(arg, ap))));
}

int		calc_unsigned(t_params *param, va_list ap, t_args **args)
{
	t_args	*arg;

	arg = push_back_new_arg(args);
	if (param->type == 'U' || param->size == 'l')
		arg->type = ULINT;
	else if (param->type == 'u' && param->size == 'L')
		arg->type = ULLINT;
	else if (param->type == 'u' && (param->size == 'h' || param->size == 'H'))
	{
		arg->type = UINT;
		if (param->size == 'h')
			arg->stype = USINT;
		else
			arg->stype = UCHAR;
	}
	else if (param->type == 'u' && param->size == 'j')
		arg->type = UINTMAX_T;
	else if (param->type == 'u' && param->size == 'z')
		arg->type = SIZE_T;
	else if (param->type == 'u')
		arg->type = UINT;
	if (arg->type == INT)
		return ((arg->len = ft_intmax_len(save_intmax_arg(arg, ap))));
	return ((arg->len =
				ft_uintmax_base_len(save_uintmax_arg(arg, ap, param), 10)));
}

int		calc_bin(t_params *param, va_list ap, t_args **args)
{
	t_args *arg;

	arg = push_back_new_arg(args);
	arg->type = UINTMAX_T;
	arg->len = ft_uintmax_base_len(save_uintmax_arg(arg, ap, param), 2);
	return (arg->len);
}

int		calc_octal(t_params *param, va_list ap, t_args **args)
{
	t_args	*arg;

	arg = push_back_new_arg(args);
	if (param->type == 'O' || param->size == 'l')
		arg->type = ULINT;
	else if (param->type == 'o' && param->size == 'L')
		arg->type = ULLINT;
	else if (param->type == 'o' && (param->size == 'h' || param->size == 'H'))
	{
		arg->type = UINT;
		if (param->size == 'h')
			arg->stype = USINT;
		else
			arg->stype = UCHAR;
	}
	else if (param->type == 'o' && param->size == 'j')
		arg->type = UINTMAX_T;
	else if (param->type == 'o' && param->size == 'z')
		arg->type = SIZE_T;
	else if (param->type == 'o')
		arg->type = UINT;
	if (arg->type == INT)
		return ((arg->len = ft_intmax_len(save_intmax_arg(arg, ap))));
	return ((arg->len =
					ft_uintmax_base_len(save_uintmax_arg(arg, ap, param), 8)));
}

int		calc_hexa(t_params *param, va_list ap, t_args **args)
{
	t_args	*arg;

	arg = push_back_new_arg(args);
	if (param->size == 'l')
		arg->type = ULINT;
	else if (param->size == 'L')
		arg->type = ULLINT;
	else if (param->size == 'h' || param->size == 'H')
	{
		arg->type = UINT;
		if (param->size == 'h')
			arg->stype = USINT;
		else
			arg->stype = UCHAR;
	}
	else if (param->size == 'j')
		arg->type = UINTMAX_T;
	else if (param->size == 'z')
		arg->type = SIZE_T;
	else
		arg->type = UINT;
	if (arg->type == INT)
		return ((arg->len = ft_intmax_len(save_intmax_arg(arg, ap))));
	return ((arg->len =
					ft_uintmax_base_len(save_uintmax_arg(arg, ap, param), 16)));
}
