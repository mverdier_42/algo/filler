/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_board.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/05 13:23:35 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/13 18:34:12 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILL_BOARD_H
# define FILL_BOARD_H

# include "parsing.h"

# define ABS(x) ((x < 0) ? -x : x)

void	get_coords(t_round *rd);
int		fill_to_end(t_round *rd);
int		to_next_star(t_round *rd);
void	calc_min_dist(t_round *rd);

int		is_out_of_board(t_round *rd, int x, int y);
int		is_out_of_board_x(t_round *rd, int x, int y);
int		is_out_of_board_y(t_round *rd, int x, int y);
int		is_in_collision(t_round *rd, int *collision, int x, int y);
int		empty_is_mine(t_round *rd);

#endif
