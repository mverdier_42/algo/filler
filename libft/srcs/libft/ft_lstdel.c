/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 19:20:20 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/20 19:19:11 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <string.h>

void	ft_lstdel(t_list **alst, void (*del)(void *))
{
	t_list *tmp;
	t_list *temp;

	tmp = *alst;
	while ((*alst)->next)
	{
		(*del)((*alst)->content);
		temp = *alst;
		free(*alst);
		*alst = temp->next;
	}
	(*del)((*alst)->content);
	free(*alst);
	*alst = NULL;
}
