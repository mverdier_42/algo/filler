/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   del.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/05 18:41:21 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/05 18:43:07 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"

void		del_piece(t_round *rd)
{
	int		i;

	i = 0;
	while (i < rd->px)
		free(rd->piece[i++]);
	free(rd->piece);
}

void		del_board(t_round *rd)
{
	int		i;

	i = 0;
	while (i < rd->bx)
		free(rd->board[i++]);
	free(rd->board);
}
