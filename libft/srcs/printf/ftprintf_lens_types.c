/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_lens_types.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 16:00:19 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 16:00:31 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_lens.h"

int		bit_len(wchar_t c)
{
	int		i;

	i = 1;
	while ((c = c >> 1))
		i++;
	return (i);
}

int		ft_intmax_len(intmax_t nb)
{
	int		len;

	len = 1;
	if (nb < 0)
	{
		if (nb == (intmax_t)-9223372036854775808ULL)
		{
			nb = -922337203685477580;
			len++;
		}
		nb = -nb;
		len++;
	}
	while (nb > 9)
	{
		nb /= 10;
		len++;
	}
	return (len);
}

int		ft_uintmax_base_len(uintmax_t nb, unsigned int base)
{
	int		len;

	len = 1;
	while (nb >= base)
	{
		nb /= base;
		len++;
	}
	return (len);
}
