# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/19 18:16:41 by mverdier          #+#    #+#              #
#    Updated: 2017/01/14 15:37:53 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = mverdier.filler

SRC = main.c parsing.c fill_board.c fill_board_verifs.c del.c fill_board_strat.c

SRCDIR = ./srcs

OBJ = $(SRC:%.c=$(OBJDIR)/%.o)

OBJDIR = ./objs

#------------------------------------------------------------------------------#

INCDIR = ./includes

LIBINCDIR = ./libft/includes

LIBDIR = ./libft

#------------------------------------------------------------------------------#

VNAME = ./visualisateur/visualisateur.filler

VSRC = visualisateur.c

#------------------------------------------------------------------------------#

CC = gcc

CFLAGS = -Wall   \
		 -Wextra \
		 -Werror

INCFLAGS = -I $(INCDIR)     \
		   -I $(LIBINCDIR)

LFLAGS = -L $(LIBDIR) -lft

FLAGS = $(CFLAGS)   \
		$(INCFLAGS)

#------------------------------------------------------------------------------#

all: libft

libft:
	@$(MAKE) -C ./libft all
	@$(MAKE) $(NAME)

$(NAME): $(OBJ) visualisateur
	@$(CC) $(FLAGS) $(LFLAGS) $(OBJ) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@$(CC) $(FLAGS) -o $@ -c $<

visualisateur: $(VNAME)

$(VNAME): $(SRCDIR)/$(VSRC)
	@$(CC) $(FLAGS) $(LFLAGS) -lncurses $< -o $@

clean:
	@rm -rf $(OBJDIR)
	@$(MAKE) -C ./libft clean

fclean: clean
	@rm -rf $(NAME)
	@rm -rf $(VNAME)
	@$(MAKE) -C ./libft fclean

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#

no:
	@echo "\033[1;32mPassage de la norminette : \033[1;31m"
	@norminette $(SRCDIR) $(INCDIRS) | grep -B1 Error | cat

printf:
	@echo "\033[1;32mVerifications des printf : \033[1;31m"
	@cat $(SRC) | grep printf | cat

check: no printf

git:
	@git add Makefile $(SRCDIR) $(INCDIR) auteur resources libft visualisateur

.PHONY: all clean re fclean git no printf check visualisateur libft
