/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualisateur.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/11 18:01:06 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/11 18:04:46 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VISUALISATEUR_H
# define VISUALISATEUR_H

# include <ncurses.h>

typedef struct	s_env
{
	WINDOW	*board;
	WINDOW	*piece;
	WINDOW	*score;
	char	buff[999];
	int		end;
}				t_env;

#endif
