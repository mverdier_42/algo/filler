/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_fill.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:58:48 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:58:58 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_fill.h"

static int	to_end_of_flags(const char *format, int i, t_params *param)
{
	while (format[i] && format[i] != param->type)
		i++;
	return (i + 1);
}

int			fill_buf(const char *format, char *buf,
		t_params *params, t_args *args)
{
	int			i;
	int			j;
	int			len;

	i = 0;
	j = 0;
	len = ft_strlen(format);
	while (i < len)
	{
		if (format[i] == '%')
		{
			if ((fill_flags(buf, &j, args, params) == -1))
				return (j);
			i = to_end_of_flags(format, i + 1, params);
			if (args)
				args = args->next;
			if (params)
				params = params->next;
		}
		else
			buf[j++] = format[i++];
	}
	return (j - 1);
}
